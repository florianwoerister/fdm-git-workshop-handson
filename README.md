# FDM Workshop "Introduction to Git"
This repository contains a summary of the hands-on session and additional material for further reading of the "Introduction to Git" Workshop held on the _7th of December 2021_. As most universities use GitLab to provide a Git Repository Service for researchers and students we used this product for this workshop as well.

As this was only a 1,5h session, we want to emphasize that this workshop is not a full-blown `git` tutorial. Our aim is to convey the philosophy behind `git` and give an overview of the most relevant features `git` and GitLab provide.

If you have questions related to this material, please don't hasitate to contact me:

_Florian Wörister_ - _Center for Research Data Management_ - florian.woerister[at]tuwien.ac.at

## Table of Content

1. [Training Material](#training-material)
2. [Let's get started](#lets-get-started)
3. [Demo Story Line](#demo-story-line)

## Training Material

| Link | Description |
| ------ | ------ |
| https://learngitbranching.js.org/ | An interactive tutorial on learning the `git`-commands |
| https://wac-cdn.atlassian.com/dam/jcr:e7e22f25-bba2-4ef1-a197-53f46b6df4a5/SWTM-2088_Atlassian-Git-Cheatsheet.pdf | A cheat sheet for the most common git commands |
| https://swcarpentry.github.io/git-novice/ | A tutorial for people that are new to git |

## Let's get started!

### What is the difference between "git" and "GitLab"?
When starting to learn about `git`, you might notice that sometimes people talk about `git` and sometimes they use terms like _GitLab_. Aren't they the same thing? The answer is no.

**Git** is a version control tool to handle a set of files.

**GitLab** is a platform that is using *git* under the hood. It acts as the repository server; every project member interacts with. With GitLab users can access all features of `git` via a WebUI. It also offers some additional features like tools to manage issues.

### The `git`-Glossary

| Term 			| Explanation 	|
| ------ 		| ------ 		|
| Repository 	| A git Repository houses your whole project including all changes that were applied on the files inside this project. |
| Commit 		| A commit is a bunch of changes that were applied to your git repository. The changes can affect several files. Each commit has at least one predecessor (except the initial commit). The resulting graph represents the change history of the git repository |
| Branch 		| A branch is a pointer that points to a certain commit. For a more detailed description have a look at the [documentation](https://git-scm.com/docs/git-branch).|
| Merge 		| Merging branch A into branch B causes that all commits, that were added to A after it was branched away from B are incorporated into Branch B. For a more detailed description have a look at the [documentation](https://git-scm.com/docs/git-merge).|
| Merge Request | Merge Requests (aka Pull Request) can be seen as tickets that a contributor of a repository creates. The ticket itself indicates, that the contributor wants branch A to be merged into branch B. It is possible to assign these tickets to a reviewer. This reviewer can check the changes before they are merged into branch B. If there are things to improve on branch A the reviewer can leaf comments on the Merge Request. In that case the contributor gets notified to fix the comments.				|

### How do i install `git` on my computer?
A detailed instruction on how to install `git` on your PC can be found [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

**Optional: Suggested UI Clients**
- [Sourcetree (Windows)](https://www.sourcetreeapp.com)
- [Gitkraken (Linux, MacOS, Windows)](https://www.gitkraken.com/)

### First Orientation

#### Projects View
![](/images/orientation_project.png)

**1** This "+" button is comes in handy whenever you want to create something (e.g. a new repository). Clicking on it will open a dropdown box with different "things" that can be created on this page.

**2** This notification area is important when you work with issues and merge requests because this "tickets" can be assigned to members of the project. If you get assigned a small number will appear next to the according icons.

**3** This button can be used to create a new repository (as alternative to using the "+" button of number 1)

**4** This textbox can be used to filter the repositories displayed on this page

**5** This is a list of all your repositories (your own and repositories you are contributing to)

#### Repository View

![](/images/orientation_repository.png)

**1** This is sidebar contains the most common features like creating a new branch, creating a merge request, etc. On small screens this sidebar is a bit confusing as it only shows the icons which are not very intuitive. By pressing the button on the bottom (number 9) the sidebar collapses and the icon descriptions are displayed.

**2** Name of the repository

**3** Description of the repository

**4** A dropbox that can be used to switch from one branch to another. This will update the page and display the project state of the selected branch.

**5** Clicking on this button will reveal the address to this repository. This address is needed to clone the repository to your local machine.

**6** This area shows information about the most recent commit of this branch (e.g. author, commit message, etc.)

**7** This area is similar to the tree view of a local file system (e.g. windows explorer). It shows the most recent state of the selected branch (as defined in dropbox indicated by number 4)

**8** This area is a file preview of the README file if present.

**9** Button for opening the sidebar menu


### Creating a GitLab Repository
Before we can start managing our files with GitLab, we must create a new repository. This can be done by pressing the "New Repository" button on the "Projects Overview" page (as depicted in the screenshot below).

![](/images/create_new_repository_1.png)

GitLab needs some information before it can create your new repository (e.g. project name). In the screenshot below you can see the form, were the information about your new GitLab repository can be provided.

![](/images/create_new_repository_2.png)

After clicking the "Create Project" Button you are ready to go 🎉

## Demo Story Line

**The scenario we are trying to solve:**

*After going through the README file, we discovered, that there is a section missing that lists additional training material. We have a bunch of links we want to add to that page. In addition, we are not allowed to add changes to the main branch of this repository because the person curating this repository wants to personally check each contribution before it is added… how are we going to solve this situation that??*

*🥁 drum roll ...*

**The suggested solution:**

1. Clone the repository to local machine
2. Create new branch “add-trainings-material”
3. Commit trainings material to branch “add-trainings-material”
4. Push changes back to GitLab Server
5. Create a “Merge Request”
6. Review “Merge Request”
7. Merge “add-trainings-material” into “main”


### Clone Repository to Local Machine --> Commit Changes --> Push Changes back to GitLab

In general, every git command has the same form `git <verb> <options>` where verb is the operation we want to execute (e.g. clone or commit) and options is additional information the command needs for the execution. First, we want to clone this repository to our local machine. This command needs to know the address of our repository. On the overview image of the [repository view](#repository-view) you can find explanations on how to find this address (i.e. pressing the blue `clone` button).

`git clone git@gitlab.com:florianwoerister/fdm-git-workshop-handson.git`

Next, we need to tell git that we want to add changes to a new branch that is called `add-trainings-material`. Notice that after creating a new branch the number of commits does not change. The new branch also points to the main branch which is the most recent commit. The `checkout` operation changes to an already existing branch. By adding the `-b` option we tell git to create a new branch if the provided branch name does not exist yet.

`git checkout -b add-training-material`

Now it's time to open our favorite text editor and add following lines into our README file:

```
## Training Material 

| Link                                      | Description                                            |
| ----------------------------------------- | ------------------------------------------------------ |
| https://learngitbranching.js.org/         | An interactive tutorial on learning the `git`-commands |
| https://swcarpentry.github.io/git-novice/ | A tutorial made for people that are new to git         |
```

Save and tell git that the changes made to this file should be included into the next commit before the changes are included into the commit. If you think that this is a cumbersome process, there is an easy explanation. 

Imagine that you work on your next publication, and you manage your latex code in git. You had a productive day and made some changes on all chapters and at the end of the day you want to commit the introduction but the changes on your conclusion are not ready to commit yet. So we need a way to tell git which changes to add to the next commit and which not to add to the next commit. This is done with the `git add` command. After executing it, the specified changes are saved in a place that is called `staging area`. When we commit changes to a branch git is not taking the changes in the local file system but rather takes the changes from this staging area to form a new commit. To add the changes in the README file, execute following command.

`git add README.md`

Finally, we can commit the changed file by executing commit. In the git world we always provide a short message for every commit that describes what we changed in this commit. In this case "new training material added" would be a suitable message. The final commit command looks like this:

`git commit -m "new training material added"`

Until now all commands only affected our local machine. No data was transferred to GitLab yet. To move our new branch to GitLab we need an operation called `push`. It allows us to specify one of our local branches that we want to send to the GitLab Server. In the git World the Server is also referred to as `origin` as this is the source were we originally fetched the repository from. If we want to push our new branch `add-training-material` to GitLab we can execute this command:

`git push origin`


### How to create Merge Requests
![](/images/create_new_mr_1.png)

![](/images/create_new_mr_2.png)

![](/images/create_new_mr_3.png)

